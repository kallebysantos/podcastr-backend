<?php

namespace App\Controller;

use App\DataTransfer\Podcast\CreatePodcastDTO;
use App\DataTransfer\Podcast\UpdatePodcastDTO;
use App\Procedure\Podcast\CreatePodcast;
use App\Procedure\Podcast\ReadPodcast;
use App\Procedure\Podcast\RemovePodcast;
use App\Procedure\Podcast\UpdatePodcast;
use App\Procedure\Podcast\UploadPodcast;
use App\Service\DropboxService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/podcast', name: 'podcast-index')]
class PodcastController extends AbstractController
{
    protected $uploadDir;

    public function __construct(string $projectDir) {
        $this->uploadDir = $projectDir."/var/upload";
    }

    #[Route('/', name: 'podcast-create', methods: ['POST'])]
    // #[IsGranted("ROLE_ADMIN")]
    public function createPodcast(CreatePodcastDTO $input): Response
    {
        try {
            $podcast = CreatePodcast::execute($input, $this->getDoctrine()->getManager());

            $result = ReadPodcast::parsePodcast($podcast);
            return $this->json($result);    
        } 
        catch (\RuntimeException $ex) {
            return $this->json(["Error" => $ex->getMessage()], 422);
        }
    }

    #[Route('/', name: 'podcast-get-all', methods: ['GET'])]
    // #[IsGranted("ROLE_USER")]
    public function getAllPodcasts(): Response
    {
        $results = ReadPodcast::getAll($this->getDoctrine()->getManager());

        return  $this->json($results);
    }

    #[Route('/{id}', name: 'podcast-get-by-id', methods: ['GET'])]
    // #[IsGranted("ROLE_USER")]
    public function getPodcastById(int $id): Response
    {
        try {
            $result = ReadPodcast::getById($id, $this->getDoctrine()->getManager());

            return  $this->json($result);
        }
        catch (\RuntimeException $ex) {
            return $this->json(["Error" => $ex->getMessage()], 404);
        }

    }

    #[Route('/{id}', name: 'podcast-update', methods: ['PUT'])]
    // #[IsGranted("ROLE_ADMIN")]
    public function updatePodcast(int $id, UpdatePodcastDTO $input): Response
    {
        try {
            $result = UpdatePodcast::execute($id, $input, $this->getDoctrine()->getManager());

            return $this->json($result);
        } 
        catch (\RuntimeException $ex) {
            return $this->json(["Error" => $ex->getMessage()], 404);
        }
    }

    #[Route('/{id}', name: 'podcast-delete', methods: ['DELETE'])]
    #[IsGranted("ROLE_ADMIN")]
    public function deletePodcast(int $id): Response
    {
        try {
            $storage = (new DropboxService)->dropbox;
            $storage->delete("/${id}");
            
            $result = RemovePodcast::execute($id, $this->getDoctrine()->getManager());
        
            return $this->json($result);
        }
        catch (\RuntimeException $ex) {
            return $this->json(["Error" => $ex->getMessage()], 404);
        }
    }

    #[Route('/upload/{id}', name: 'podcast-upload', methods: ['POST'])]
    // #[IsGranted("ROLE_ADMIN")]
    public function uploadPodcast(int $id, Request $req): Response
    {
        $cache = new FilesystemAdapter();
        $file = $req->files->get('data');
        $data = $req->request->all();
        
        $uploadCache = $cache->getItem("uploadCast-{$id}");
        $cachedData = $uploadCache->get();

        $filepath = $this->uploadDir."/{$id}-{$data['index']}.mp3";

        if(!move_uploaded_file($file->getPathName(), $filepath))
        {
            return $this->json("Unable to upload this file", 422);
        }

        $filesize = $cachedData['filesize'] ?? $data['filesize'];
        $totalOfChunks = $cachedData['count'] ?? $data['count'];
        $listOfChunks = $cachedData['list'] ?? [];

        $listOfChunks[$data['index']] = [
            "index" => $data['index'],
            "filepath" => $filepath,
            "size" => $file->getSize()
        ];
        
        $uploadCache->set([
            "filesize" => $filesize,
            "filepath" => $this->uploadDir."/{$id}.mp3",
            "count" => $totalOfChunks,
            "list" => $listOfChunks
        ]);

        $cache->save($uploadCache);

        if(count($listOfChunks) == $totalOfChunks)
        {
            $result = UploadPodcast::audio($id, $uploadCache, $this->getDoctrine()->getManager());

            return $this->json([
                "inCache" => $uploadCache,
                "result" => $result
            ]);
        }

        return $this->json([]);
    }
}
