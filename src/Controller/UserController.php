<?php

namespace App\Controller;

use App\DataTransfer\User\UpdateUserDTO;
use App\Procedure\User\ReadUser;
use App\Procedure\User\UpdateUser;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\LcobucciJWTEncoder;
use Lexik\Bundle\JWTAuthenticationBundle\TokenExtractor\AuthorizationHeaderTokenExtractor;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/user', name: 'user-index')]
class UserController extends AbstractController
{
    #[Route('/{id}', name: 'user-update', methods: ['PUT'])]
    #[IsGranted("ROLE_ADMIN")]
    public function updateUser(int $id, UpdateUserDTO $input): Response
    {
        try {
            $result = UpdateUser::execute($id, $input, $this->getDoctrine()->getManager());

            return $this->json($result);
        } 
        catch (\RuntimeException $ex) {
            return $this->json(["Error" => $ex->getMessage()], 404);
        }
    }

    #[Route('/', name: 'user-info', methods: ['GET'])]
    #[IsGranted("ROLE_USER")]
    public function getUserInfo(Request $req, JWTEncoderInterface $jwtEncoder)
    {
        try {
            $extractor = new AuthorizationHeaderTokenExtractor('Bearer', 'Authorization');

            $token = $extractor->extract($req);
            $decoded = $jwtEncoder->decode($token);
            
            $result = ReadUser::getById($decoded["id"], $this->getDoctrine()->getManager());

            return $this->json($result);
        } 
        catch (\RuntimeException $ex) {
            return $this->json(["Error" => $ex->getMessage()], 404);
        }
    }
}
