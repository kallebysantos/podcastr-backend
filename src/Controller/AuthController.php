<?php

namespace App\Controller;

use App\DataTransfer\User\CreateUserDTO;
use App\Procedure\User\CreateUser;
use App\Procedure\User\ReadUser;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

#[Route('/auth', name: 'auth')]
class AuthController extends AbstractController
{
    #[Route('/sign-up', name: 'register', methods: ['POST'])]
    public function resgister(CreateUserDTO $input, UserPasswordEncoderInterface $encoder): Response
    {
        try{
            $manager = $this->getDoctrine()->getManager();
            $user = CreateUser::execute($input, $manager, $encoder);
    
            $result = ReadUser::parseUser($user);
            return $this->json($result);
        }
        catch (\RuntimeException $ex) {
            return $this->json(["Error" => $ex->getMessage()], 422);
        }
    }
}
