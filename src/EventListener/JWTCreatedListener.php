<?php
namespace App\EventListener;

use App\Procedure\User\ReadUser;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;

class JWTCreatedListener 
{
  public function onJWTCreated(JWTCreatedEvent $event)
  {
    $user = ReadUser::parseUser($event->getUser());
  
    $payload = $event->getData();
    $payload['id'] = $user->id;

    $event->setData($payload);
  }
}