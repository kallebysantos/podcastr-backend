<?php
namespace App\EventListener;

// src/App/EventListener/AuthenticationSuccessListener.php

use App\Procedure\User\ReadUser;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Symfony\Component\Security\Core\User\UserInterface;

class AuthenticationSuccessListener
{
  /**
 * @param AuthenticationSuccessEvent $event
 */
  public function onAuthenticationSuccessResponse(AuthenticationSuccessEvent $event)
  {
    $data = $event->getData();
    $user = ReadUser::parseUser($event->getUser());

    $data['user'] = $user;

    $event->setData($data);
  }  
}