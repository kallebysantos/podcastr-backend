<?php

namespace App\DataTransfer\User;

use App\DataTransfer\BaseDataTransfer;

class ReadUserDTO extends BaseDataTransfer
{
  public int $id;

  public string $name;

  public string $email;

  public array $roles;

  public string|null $imageUrl;
}
