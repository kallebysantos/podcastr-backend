<?php

namespace App\DataTransfer\User;

use App\DataTransfer\BaseDataTransfer;
use Symfony\Component\Validator\Constraints\Length;

class UpdateUserDTO extends BaseDataTransfer
{
  #[Length(min: 5)]
  public string $name;

  #[Length(min: 5)]
  public string $password;
}
