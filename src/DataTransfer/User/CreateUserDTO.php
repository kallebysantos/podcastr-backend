<?php

namespace App\DataTransfer\User;

use App\DataTransfer\BaseDataTransfer;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class CreateUserDTO extends BaseDataTransfer
{
  #[NotBlank()]
  #[Length(min: 5)]
  public string $name;

  #[Email()]
  #[NotBlank()]
  public string $email;

  #[NotBlank()]
  #[Length(min: 5)]
  public string $password;
}
