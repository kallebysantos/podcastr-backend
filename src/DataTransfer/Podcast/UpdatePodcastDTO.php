<?php

namespace App\DataTransfer\Podcast;

use App\DataTransfer\BaseDataTransfer;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\Url;

class UpdatePodcastDTO extends BaseDataTransfer
{
  #[Length(min: 5)]
  public string $name;

  public string $duration;

  public string $description;

  public string $members;

  public string $fileId;

  #[Url()]
  public string $fileUrl;

  public string $thumbnailId;

  #[Url()]
  public string $thumbnailUrl;
}
