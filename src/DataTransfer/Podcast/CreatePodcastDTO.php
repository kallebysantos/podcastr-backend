<?php

namespace App\DataTransfer\Podcast;

use App\DataTransfer\BaseDataTransfer;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Url;

class CreatePodcastDTO extends BaseDataTransfer
{
  #[NotBlank()]
  #[Length(min: 5)]
  public string $name;

  #[NotBlank()]
  public string $duration;

  #[NotBlank()]
  public string $description;

  #[NotBlank()]
  public string $members;
}
