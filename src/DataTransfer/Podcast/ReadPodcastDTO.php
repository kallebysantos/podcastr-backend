<?php

namespace App\DataTransfer\Podcast;

use App\DataTransfer\BaseDataTransfer;

class ReadPodcastDTO extends BaseDataTransfer
{
  public int $id;

  public string $name;

  public string $duration;

  public string $description;

  public string $publishedAt;

  public string $members;

  public string|null $fileUrl;

  public string|null $thumbnailUrl;
}
