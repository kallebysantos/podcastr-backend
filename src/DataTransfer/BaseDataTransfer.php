<?php

namespace App\DataTransfer;

use App\Helper\RequestDTO\IRequestDTO;
use Symfony\Component\HttpFoundation\Request;

abstract class BaseDataTransfer implements IRequestDTO
{
  public function __construct(Request $request = null)
  {
    $data = json_decode($request?->getContent(), true);

    if (!$data) return;

    // Object assign
    foreach ($data as $property => $value) {
      if (!property_exists($this, $property)) continue;

      $this->$property = $value;
    }
  }
}
