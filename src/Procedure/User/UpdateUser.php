<?php

namespace App\Procedure\User;

use App\DataTransfer\User\ReadUserDTO;
use App\DataTransfer\User\UpdateUserDTO;
use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use RuntimeException;

class UpdateUser
{
  public static function execute(int $id, UpdateUserDTO $input, ObjectManager $manager): ReadUserDTO
  {
    $user = $manager->find(User::class, $id);

    if(is_null($user)) throw new RuntimeException("Can not find this user");

    foreach($input as $key => $value) {
      $func_name = 'set'.$key;
      if(!method_exists($user, $func_name)) continue;

      $user->$func_name($value);
    }

    $manager->flush();

    return ReadUser::parseUser($user);
  }
}
