<?php

namespace App\Procedure\User;

use App\Entity\User;
use App\DataTransfer\User\CreateUserDTO;
use Doctrine\Persistence\ObjectManager;
use Exception;
use RuntimeException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class CreateUser
{
  public static function execute(CreateUserDTO $input, ObjectManager $manager, UserPasswordEncoderInterface $encoder): User
  {
    $user = new User();

    $user
      ->setName($input->name)
      ->setEmail($input->email)
      ->setPassword(
        $encoder->encodePassword($user, $input->password)
      );
    try {
      $manager->persist($user);
      $manager->flush();

      return $user;
    } catch (Exception $ex) {
      throw new RuntimeException('Can not create this user, probably this email is already in use');
    }
  }
}
