<?php

namespace App\Procedure\User;

use App\DataTransfer\User\ReadUserDTO;
use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use RuntimeException;

class ReadUser
{
  public static function parseUser(User $user): ReadUserDTO
  {
    $result = new ReadUserDTO();
    $result->id = $user->getId();
    $result->name = $user->getName();
    $result->email = $user->getEmail();
    $result->roles = $user->getRoles();
    $result->imageUrl = $user->getImageUrl();

    return $result;
  }

  public static function getById(int $id, ObjectManager $manager): ReadUserDTO
  {
    $user = $manager->find(User::class, $id);
    if(is_null($user)) throw new RuntimeException("Can not find this entity");

    return self::parseUser($user);
  }
}
