<?php

namespace App\Procedure\Podcast;

use App\DataTransfer\Podcast\ReadPodcastDTO;
use App\DataTransfer\Podcast\UpdatePodcastDTO;
use App\Entity\Podcast;
use Doctrine\Persistence\ObjectManager;
use RuntimeException;

class UpdatePodcast
{
  public static function execute(int $id, UpdatePodcastDTO $input, ObjectManager $manager): ReadPodcastDTO
  {
    $podcast = $manager->find(Podcast::class, $id);

    if(is_null($podcast)) throw new RuntimeException("Can not find this podcast");

    foreach($input as $key => $value) {
      $func_name = 'set'.$key;
      if(!method_exists($podcast, $func_name)) continue;

      $podcast->$func_name($value);
    }

    $manager->flush();

    return ReadPodcast::parsePodcast($podcast);
  }
}
