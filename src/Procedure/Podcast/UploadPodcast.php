<?php

namespace App\Procedure\Podcast;

use App\Entity\Podcast;
use App\Service\DropboxService;
use Doctrine\Persistence\ObjectManager;
use Kunnu\Dropbox\DropboxFile;
use RuntimeException;
use Symfony\Component\Cache\CacheItem;

class UploadPodcast
{
  public static function getSharedLink(string $filepath): string|null
  {
    $storage = (new DropboxService)->dropbox;
    
    $response = $storage->postToAPI('/sharing/create_shared_link_with_settings', [
      "path" => $filepath,
    ]);

    $data = $response->getDecodedBody();
    return explode('?', $data['url'])[0] ?? null;
  }

  public static function audio(int $id, CacheItem $cache, ObjectManager $manager): Podcast
  {
    $podcast = $manager->find(Podcast::class, $id);
    if (is_null($podcast)) throw new RuntimeException("Can not find this entity");

    $cachedData = $cache->get();
    $totalOfChunks = $cachedData['count'] ?? throw new RuntimeException("Total of chunks can not be null");
    $listOfChunks = $cachedData['list'] ?? throw new RuntimeException("List of chunks not found");
    $filesize = $cachedData['filesize'] ?? throw new RuntimeException("Size of file not found");
    $filepath = $cachedData['filepath'] ?? throw new RuntimeException("Unable to open filepath");

    $storage = (new DropboxService)->dropbox;
    sort($listOfChunks);

    // Search for a folder, if it not exists will create a new
    $folder = $storage->search("/", "/${id}")?->getItems()?->first()?->getMetadata() ?? $storage->createFolder("/{$id}");

    foreach ($listOfChunks as $item) {
      $inputFile = fopen($item['filepath'], 'rb');
      $outputFile = fopen($filepath, 'a+');

      while ($buff = fread($inputFile, $item['size'])) {
        fwrite($outputFile, $buff);
      }

      fclose($inputFile);
      fclose($outputFile);
      unlink($item['filepath']);
    }

    $dropboxFile = new DropboxFile($filepath);
    $onCloudFilepath = $folder->getPathLower()."/audio.mp3";
    $uploadChunkSize = ceil($filesize / $totalOfChunks);

    $file = $storage->uploadChunked($dropboxFile, $onCloudFilepath, $filesize, $uploadChunkSize, ["autorename" => true]);

    unlink($filepath);
    
    $sharedLink = self::getSharedLink($onCloudFilepath);

    $podcast->setFileId($file->getId());
    $podcast->setFileUrl($sharedLink);

    $manager->flush();

    return $podcast;
  }
}
