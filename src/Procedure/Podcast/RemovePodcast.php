<?php

namespace App\Procedure\Podcast;

use App\Entity\Podcast;
use Doctrine\Persistence\ObjectManager;
use RuntimeException;

class RemovePodcast
{
  public static function execute(int $id, ObjectManager $manager): bool
  {
    $podcast = $manager->find(Podcast::class, $id);
    
    if(is_null($podcast)) throw new RuntimeException("This entity can not be deleted or not exists");

    $manager->remove($podcast);
    $manager->flush();

    return true;
  }
}
