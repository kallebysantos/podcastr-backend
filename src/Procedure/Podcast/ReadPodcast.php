<?php

namespace App\Procedure\Podcast;

use App\DataTransfer\Podcast\ReadPodcastDTO;
use App\Entity\Podcast;
use Doctrine\Persistence\ObjectManager;
use RuntimeException;

class ReadPodcast
{
  public static function getAll(ObjectManager $manager): array
  {
    $repository = $manager->getRepository(Podcast::class);
    $podcasts = $repository->findAll();

    return array_map('self::parsePodcast', $podcasts);
  }

  public static function getById(int $id, ObjectManager $manager): ReadPodcastDTO
  {
    $podcast = $manager->find(Podcast::class, $id);
    if(is_null($podcast)) throw new RuntimeException("Can not find this entity");

    return self::parsePodcast($podcast);
  }

  public static function parsePodcast(Podcast $podcast): ReadPodcastDTO
  {
    $result = new ReadPodcastDTO();
    $result->id = $podcast->getId();
    $result->name = $podcast->getName();
    $result->duration = $podcast->getDuration();
    $result->description = $podcast->getDescription();
    $result->publishedAt = $podcast->getPublishedAt();
    $result->members = $podcast->getMembers();
    $result->fileUrl = $podcast?->getFileUrl();
    $result->thumbnailUrl = $podcast?->getThumbnailUrl();

    return $result;
  }
}
