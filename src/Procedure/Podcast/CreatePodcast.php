<?php

namespace App\Procedure\Podcast;

use App\DataTransfer\Podcast\CreatePodcastDTO;
use App\Entity\Podcast;
use Doctrine\Persistence\ObjectManager;

class CreatePodcast
{
  public static function execute(CreatePodcastDTO $input, ObjectManager $manager): Podcast
  {
    $date = date("Y-m-d H:i:s");

    $podcast = (new Podcast())
      ->setName($input->name)
      ->setDuration($input->duration)
      ->setDescription($input->description)
      ->setMembers($input->members)
      ->setPublishedAt($date);

    $manager->persist($podcast);
    $manager->flush();

    return $podcast;
  }
}
