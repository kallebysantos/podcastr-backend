<?php
namespace App\Helper\RequestDTO;

use Symfony\Component\HttpFoundation\Request;

interface IRequestDTO
{
  public function __construct(Request $request);
}