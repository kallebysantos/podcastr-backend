FROM kallebysantos/symfonycli

RUN apt-get update && apt-get install -y libpq-dev
RUN docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql

RUN docker-php-ext-install pdo pdo_mysql pdo_pgsql

WORKDIR /app

COPY . .

CMD [ "symfony", "serve" ]
