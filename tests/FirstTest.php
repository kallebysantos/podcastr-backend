<?php

namespace App\Tests;

use App\Tests\DatabasePrimer;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class FirstTest extends KernelTestCase
{
  private EntityManager $EntityManager;

  protected function setUp(): void
  {
    // Comment the line bellow to perform a environment test
    $this->markTestSkipped("Just be sure that it works");

    $kernel = self::bootKernel();

    DatabasePrimer::prime($kernel);

    $this->EntityManager = $kernel->getContainer()->get('doctrine')->getManager();
  }

  protected function tearDown(): void
  {
    $kernel = self::$kernel;
    $this->EntityManager->close();

    DatabasePrimer::drop($kernel);
    parent::tearDown();
  }

  /** @test */
  public function should_pass()
  {
    $this->assertTrue(true);
  }

  /** @test */
  public function should_get_error()
  {
    $this->assertTrue(false);
  }
}
