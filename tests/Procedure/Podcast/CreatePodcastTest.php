<?php
namespace App\Tests\Procedure\Podcast;

use App\DataTransfer\Podcast\CreatePodcastDTO;
use App\Entity\Podcast;
use App\Procedure\Podcast\CreatePodcast;
use App\Tests\DatabasePrimer;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CreatePodcastTest extends KernelTestCase
{
  private EntityManager $EntityManager;

  protected function setUp(): void
  {
    $kernel = self::bootKernel();
    DatabasePrimer::prime($kernel);

    $this->EntityManager = $kernel->getContainer()->get('doctrine')->getManager();
  }

  protected function tearDown(): void
  {
    $kernel = self::$kernel;
    $this->EntityManager->close();

    DatabasePrimer::drop($kernel);
    parent::tearDown();
  }

  /** @test */
  public function should_create_a_podcast()
  {
    $repository = $this->EntityManager->getRepository(Podcast::class);

    $input = new CreatePodcastDTO();
    $input->name = "First podcast";
    $input->duration = "3600";
    $input->description = "Um podcast para testar a aplicação";
    $input->members = "Kalleby, Mauricio, Kazimir";
/*  $input->fileId = "id:vIutYSx7hnAAAAAAAAAAlg";
    $input->fileUrl = "https://www.dropbox.com/s/lt4h64ndbg1hzva/audio.mp3";
    $input->thumbnailId = "id:vIutYSx7hnAAAAAAAAAAlw";
    $input->thumbnailUrl = "https://www.dropbox.com/s/b3h2kmcbs4ek2ma/thumbnail.png"; */

    $podcast = CreatePodcast::execute($input, $this->EntityManager);
    $record = $repository->findOneBy(["id" => 1]);

    $this->assertEquals($podcast->getId(), $record->getId());
    $this->assertEquals($podcast->getName(), $record->getName());
    $this->assertEquals($podcast->getDuration(), $record->getDuration());
    $this->assertEquals($podcast->getDescription(), $record->getDescription());
    $this->assertEquals($podcast->getPublishedAt(), $record->getPublishedAt());
    $this->assertEquals($podcast->getMembers(), $record->getMembers());

    $this->assertEquals($podcast->getFileId(), $record->getFileId());
    $this->assertEquals($podcast->getFileUrl(), $record->getFileUrl());
    $this->assertEquals($podcast->getThumbnailId(), $record->getThumbnailId());
    $this->assertEquals($podcast->getThumbnailUrl(), $record->getThumbnailUrl());
  }
}