<?php
namespace App\Tests\Procedure\Podcast;

use App\DataTransfer\Podcast\ReadPodcastDTO;
use App\Entity\Podcast;
use App\Procedure\Podcast\ReadPodcast;
use App\Tests\DatabasePrimer;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class GetPodcastsTest extends KernelTestCase
{
  private EntityManager $EntityManager;

  protected function setUp(): void
  {
    $kernel = self::bootKernel();
    DatabasePrimer::prime($kernel);

    $this->EntityManager = $kernel->getContainer()->get('doctrine')->getManager();
  }

  protected function tearDown(): void
  {
    $this->EntityManager->close();
    parent::tearDown();
  }

  /** @afterClass */
  public static function tearDownDatabase(): void
  {
    $kernel = self::bootKernel();

    DatabasePrimer::drop($kernel);
  }

  /** 
   *  @test
   *  @doesNotPerformAssertions
  */
  public function setUpEntity(): Podcast
  {
    $date = date("Y-m-d H:i:s");

    $podcast = (new Podcast())
      ->setName("My first podcast")
      ->setDuration(4500)
      ->setDescription("A first podcast to tests")
      ->setMembers("Kalleby Santos")
      ->setPublishedAt($date)
      ->setFileId("abc")
      ->setFileUrl("file.mp3")
      ->setThumbnailId("cba")
      ->setThumbnailUrl("thumb.png");

    $this->EntityManager->persist($podcast);
    $this->EntityManager->flush();
    return $podcast;
  }

  /** 
   * @test
   * @depends setUpEntity
  */
  public function should_get_all_podcast(Podcast $podcast)
  {    
    $record = ReadPodcast::getAll($this->EntityManager)[0];

    $this->assertEquals($record::class, ReadPodcastDTO::class);

    $this->assertEquals($podcast->getId(), $record->id);
    $this->assertEquals($podcast->getName(), $record->name);
    $this->assertEquals($podcast->getDuration(), $record->duration);
    $this->assertEquals($podcast->getDescription(), $record->description);
    $this->assertEquals($podcast->getPublishedAt(), $record->publishedAt);
    $this->assertEquals($podcast->getMembers(), $record->members);
    $this->assertEquals($podcast->getFileUrl(), $record->fileUrl);
    $this->assertEquals($podcast->getThumbnailUrl(), $record->thumbnailUrl);
  }

  /** 
   * @test
   * @depends setUpEntity
  */
  public function should_get_a_podcast_by_id(Podcast $podcast)
  {
    $record = ReadPodcast::getById($podcast->getId(), $this->EntityManager);

    $this->assertEquals($record::class, ReadPodcastDTO::class);

    $this->assertEquals($podcast->getId(), $record->id);
    $this->assertEquals($podcast->getName(), $record->name);
    $this->assertEquals($podcast->getDuration(), $record->duration);
    $this->assertEquals($podcast->getDescription(), $record->description);
    $this->assertEquals($podcast->getPublishedAt(), $record->publishedAt);
    $this->assertEquals($podcast->getMembers(), $record->members);
    $this->assertEquals($podcast->getFileUrl(), $record->fileUrl);
    $this->assertEquals($podcast->getThumbnailUrl(), $record->thumbnailUrl);
  }
}