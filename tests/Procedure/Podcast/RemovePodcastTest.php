<?php

namespace App\Tests\Procedure\Podcast;

use App\Entity\Podcast;
use App\Procedure\Podcast\RemovePodcast;
use App\Tests\DatabasePrimer;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class RemovePodcastTest extends KernelTestCase
{
  private EntityManager $EntityManager;

  protected function setUp(): void
  {
    $kernel = self::bootKernel();
    DatabasePrimer::prime($kernel);

    $this->EntityManager = $kernel->getContainer()->get('doctrine')->getManager();
  }

  protected function tearDown(): void
  {
    $this->EntityManager->close();
    parent::tearDown();
  }

  /** @afterClass */
  public static function tearDownDatabase(): void
  {
    $kernel = self::bootKernel();

    DatabasePrimer::drop($kernel);
  }

  /** 
   *  @test
   *  @doesNotPerformAssertions
  */
  public function setUpEntity(): Podcast
  {
    $date = date("Y-m-d H:i:s");

    $podcast = (new Podcast())
      ->setName("My first podcast")
      ->setDuration(4500)
      ->setDescription("A first podcast to tests")
      ->setMembers("Kalleby Santos")
      ->setPublishedAt($date)
      ->setFileId("abc")
      ->setFileUrl("file.mp3")
      ->setThumbnailId("cba")
      ->setThumbnailUrl("thumb.png");

    $this->EntityManager->persist($podcast);
    $this->EntityManager->flush();
    return $podcast;
  }

  /** 
   * @test
   * @depends setUpEntity
  */
  public function should_remove_a_podcast(Podcast $podcast)
  {    
    RemovePodcast::execute($podcast->getId(), $this->EntityManager);

    $repository = $this->EntityManager->getRepository(Podcast::class);
    $record = $repository->findOneBy(["id" => $podcast->getId()]);

    $this->assertNull($record);
  }
}