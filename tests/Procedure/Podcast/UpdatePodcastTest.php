<?php
namespace App\Tests\Procedure\Podcast;

use App\DataTransfer\Podcast\UpdatePodcastDTO;
use App\Entity\Podcast;
use App\Procedure\Podcast\UpdatePodcast;
use App\Tests\DatabasePrimer;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UpdatePodcastTest extends KernelTestCase
{
  private EntityManager $EntityManager;

  protected function setUp(): void
  {
    $kernel = self::bootKernel();
    DatabasePrimer::prime($kernel);

    $this->EntityManager = $kernel->getContainer()->get('doctrine')->getManager();
  }

  protected function tearDown(): void
  {
    $this->EntityManager->close();
    parent::tearDown();
  }

  /** @afterClass */
  public static function tearDownDatabase(): void
  {
    $kernel = self::bootKernel();

    DatabasePrimer::drop($kernel);
  }

  /** 
   *  @test
   *  @doesNotPerformAssertions
  */
  public function setUpEntity(): Podcast
  {
    $date = date("Y-m-d H:i:s");

    $podcast = (new Podcast())
      ->setName("My first podcast")
      ->setDuration(4500)
      ->setDescription("A first podcast to tests")
      ->setMembers("Kalleby Santos")
      ->setPublishedAt($date)
      ->setFileId("abc")
      ->setFileUrl("file.mp3")
      ->setThumbnailId("cba")
      ->setThumbnailUrl("thumb.png");

    $this->EntityManager->persist($podcast);
    $this->EntityManager->flush();
    return $podcast;
  }

  /** 
   * @test
   * @depends setUpEntity
  */
  public function should_update_a_podcast(Podcast $podcast)
  {
    $input = new UpdatePodcastDTO();
    $input->name = "My updated podcast";
    $input->duration = 3600;
    $input->description = "Um podcast para testar a aplicação";
    $input->members = "Kalleby, Mauricio, Kazimir";

    $updated = UpdatePodcast::execute($podcast->getId(), $input, $this->EntityManager);
    $record = $this->EntityManager->find(Podcast::class, $podcast->getId());

    // Verify if is updated on database
    $this->assertEquals($record->getId(), $updated->id);
    $this->assertEquals($record->getName(), $updated->name);
    $this->assertEquals($record->getDuration(), $updated->duration);
    $this->assertEquals($record->getDescription(), $updated->description);
    $this->assertEquals($record->getPublishedAt(), $updated->publishedAt);
    $this->assertEquals($record->getMembers(), $updated->members);

    // Verify if it has changed
    $this->assertNotEquals($podcast->getName(), $updated->name);
    $this->assertNotEquals($podcast->getDuration(), $updated->duration);
    $this->assertNotEquals($podcast->getDescription(), $updated->description);
    $this->assertNotEquals($podcast->getMembers(), $updated->members);
  }
}