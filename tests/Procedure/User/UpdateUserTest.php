<?php
namespace App\Tests\Procedure\User;

use App\DataTransfer\User\UpdateUserDTO;
use App\Entity\User;
use App\Procedure\User\UpdateUser;
use App\Tests\DatabasePrimer;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UpdateUserTest extends KernelTestCase
{
  private EntityManager $EntityManager;

  protected function setUp(): void
  {
    $kernel = self::bootKernel();
    DatabasePrimer::prime($kernel);

    $this->EntityManager = $kernel->getContainer()->get('doctrine')->getManager();
  }

  protected function tearDown(): void
  {
    $this->EntityManager->close();
    parent::tearDown();
  }

  /** @afterClass */
  public static function tearDownDatabase(): void
  {
    $kernel = self::bootKernel();

    DatabasePrimer::drop($kernel);
  }

  /** 
   *  @test
   *  @doesNotPerformAssertions
  */
  public function setUpEntity(): User
  {
    $user = (new User())
      ->setName("My first podcast")
      ->setEmail('kalleby@mail.com')
      ->setPassword("123456789")
      ->setImageId("abc123")
      ->setImageUrl("image.png");

    $this->EntityManager->persist($user);
    $this->EntityManager->flush();
    return $user;
  }

  /** 
   * @test
   * @depends setUpEntity
  */
  public function should_update_a_podcast(User $user)
  {
    $input = new UpdateUserDTO();
    $input->name = "My updated podcast";
    $input->password = '123456';

    $updated = UpdateUser::execute($user->getId(), $input, $this->EntityManager);
    $record = $this->EntityManager->find(User::class, $user->getId());

    // Verify if is updated on database
    $this->assertEquals($record->getId(), $updated->id);
    $this->assertEquals($record->getName(), $updated->name);
    $this->assertEquals($record->getEmail(), $updated->email);

    // Verify if it has changed
    $this->assertNotEquals($user->getName(), $record->getName());
    $this->assertNotEquals($user->getPassword(), $record->getPassword());
  }
}