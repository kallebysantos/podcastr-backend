<?php
namespace App\Tests\Procedure\User;

use App\Entity\User;
use App\DataTransfer\User\CreateUserDTO;
use App\Procedure\User\CreateUser;
use App\Tests\DatabasePrimer;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class CreateUserTest extends KernelTestCase
{
  private EntityManager $EntityManager;
  private UserPasswordEncoderInterface $passwordEncoder;

  protected function setUp(): void
  {
    $kernel = self::bootKernel();
    DatabasePrimer::prime($kernel);

    $this->EntityManager = $kernel->getContainer()->get('doctrine')->getManager();
    $this->passwordEncoder = $kernel->getContainer()->get('security.password_encoder');
  }

  protected function tearDown(): void
  {
    $this->EntityManager->close();
    parent::tearDown();
  }

  /** @afterClass */
  public static function tearDownDatabase(): void
  {
    $kernel = self::bootKernel();

    DatabasePrimer::drop($kernel);
  }

  /** @test */
  public function should_create_a_user()
  {
    $repository = $this->EntityManager->getRepository(User::class);

    $input = new CreateUserDTO();
    $input->name = "kalleby Santos";
    $input->email = "kalleby@email.com";
    $input->password = "123456789";

    $user = CreateUser::execute($input, $this->EntityManager, $this->passwordEncoder);
    $record = $repository->findOneBy(["email" => $input->email]);

    $this->assertEquals($user->getId(), $record->getId());
    $this->assertEquals($user->getName(), $record->getName());
    $this->assertEquals($user->getEmail(), $record->getEmail());
    $this->assertEquals($user->getPassword(), $record->getPassword());
  }

  /** @test */
  public function should_failed_on_create_with_same_email()
  {
    $this->expectException('RuntimeException');

    $input = new CreateUserDTO();
    $input->name = "kalleby Santos";
    $input->email = "kalleby@email.com";
    $input->password = "123456789";

    CreateUser::execute($input, $this->EntityManager, $this->passwordEncoder);
  }

}